
$(function() {
    $('.card-a').hover(function() {
      $('.greenline-a').css('background-color', '#00e8a4');
      $('.card-1').attr("src", "images/bt-branding-on.png");
    }, function() {
      $('.greenline-a').css('background-color', 'white');
      $('.card-1').attr('src', 'images/bt-branding-off.png');
    });
  });

  $(function() {
    $('.card-b').hover(function() {
      $('.greenline-b').css('background-color', '#00e8a4');
      $('.card-2').attr("src", "images/bt-design-on.png");
    }, function() {
      $('.greenline-b').css('background-color', 'white');
      $('.card-2').attr('src', 'images/bt-design-off.png');
    });
  });

  $(function() {
    $('.card-c').hover(function() {
      $('.greenline-c').css('background-color', '#00e8a4');
      $('.card-3').attr("src", "images/bt-web-on.png");
    }, function() {
      $('.greenline-c').css('background-color', 'white');
      $('.card-3').attr('src', 'images/bt-web-off.png');
    });
  });

  $(function() {
    $('.card-d').hover(function() {
      $('.greenline-d').css('background-color', '#00e8a4');
      $('.card-4').attr("src", "images/bt-social-on.png");
    }, function() {
      $('.greenline-d').css('background-color', 'white');
      $('.card-4').attr('src', 'images/bt-social-off.png');
    });
  });

  var $button = document.querySelector('.button');
$button.addEventListener('click', function() {
  var duration = 0.3,
      delay = 0.08;
  TweenMax.to($button, duration, {scaleY: 1.6, ease: Expo.easeOut});
  TweenMax.to($button, duration, {scaleX: 1.2, scaleY: 1, ease: Back.easeOut, easeParams: [3], delay: delay});
  TweenMax.to($button, duration * 1.25, {scaleX: 1, scaleY: 1, ease: Back.easeOut, easeParams: [6], delay: delay * 3 });
});

